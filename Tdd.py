import unittest
from tag import ler
import sys

class TestStringMethods(unittest.TestCase):
	def test_classifica(self):
		corrigido = ler(sys.stdin)
		flag = False  	 
		if '<' not in corrigido and '>' not in corrigido:
			flag = True

		self.assertEqual(flag , True)

	
		




def runTests():
	suite = unittest.defaultTestLoader.loadTestsFromTestCase(TestStringMethods)
	unittest.TextTestRunner(verbosity=2, failfast=True).run(suite)

if __name__ == '__main__':
	unittest.main()
